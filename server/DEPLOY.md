# Deploy via deploy.js

## Usage

To deploy new PWAs, run:
```
docker-compose build
docker-compose up
```

Inside the running Docker container, run:
```
yarn deploy
```

## To do

* Get JSON;
* Create new Netlify site via API;
* Remove nuxt-sass-resources-loader;
* Create function to update PWA after user updates PWA or Page;
* https://www.netlify.com/docs/api/#deploying-to-netlify

## How does it work

* Receives JSON with edition names and colors;
* Creates a .scss file named after the edition with respective colors;
* Creates .vue file based on ```_deploy_edition.vue```, importing respective .scss inside ```<style>``` tag;
* Runs ```nuxt generate```, which creates a ```dist``` folder;
* Zips ```dist``` folder using [zip-folder](https://github.com/sole/node-zip-folder);
* Reads zip as binary with [file-api](https://www.npmjs.com/package/file-api);
* Sends it via [axios](https://github.com/axios/axios) post to [Netlify API](https://www.netlify.com/docs/api/);
* [Netlify](www.netlify.com) receives post and deploys on [test3.hacknizer.tk](test3.hacknizer.tk/edition1]) (hardcoded test site).