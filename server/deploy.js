const fs = require("fs-extra")
const { Nuxt, Builder, Generator } = require("nuxt")

const { post } = require("axios")
// const { exec } = require('child_process');

const { File, FileReader } = require("file-api")
const zip = require("zip-folder")

const config = require("../nuxt.config.js")
config.dev = false

const nuxt = new Nuxt(config)
const generator = new Generator(nuxt, new Builder(nuxt))

const JSON = {
  edition1: {
    "palette-primary": "black",
    "palette-secondary": "black"
  },
  edition2: {
    "palette-primary": "cyan",
    "palette-secondary": "cyan"
  },
  edition3: {
    "palette-primary": "red",
    "palette-secondary": "red"
  },
  edition4: {
    "palette-primary": "blue",
    "palette-secondary": "blue"
  }
}

const STYLE_HEADER = "\n\n<style lang='scss'>\n"
const STYLE_ENDING = "@import '../assets/global.scss';\n" + "</style>"

const site_id = "21ba9f12-edcf-4354-8cd6-39e787863d42"

async function deploy() {
  // Iterates JSON and creates a Sass and a Vue file for each edition
  for (edition in JSON) {
    const sassFilePath = await createSassFile(edition)
    await createVueFile(edition, sassFilePath)
  }

  // Run nuxt generate
  await generator.generate()

  // Zips dist file
  zip("./dist", "dist.zip", async err => {
    if (err) {
      console.log("ZIP failed: ", err)
    } else {
      console.log("Successfully zipped file!")
      let file = new File("./dist.zip")
      const data = await fileReader(file)
      try {
        const res = await post(
          `https://api.netlify.com/api/v1/sites/${site_id}/deploys`,
          data,
          {
            headers: {
              Authorization: "Bearer " + process.env.NETLIFY_ACCESS_TOKEN,
              "Content-Type": "application/zip"
            }
          }
        )
        console.log(res)
        fs.unlink("./dist.zip")
        fs.remove("./dist")
      } catch (e) {
        console.log(e)
      }
    }
  })

  // Deletes generated files
  afterDeploy()
}

function fileReader(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = e => {
      resolve(reader.result)
    }
    reader.readAsArrayBuffer(file)
  })
}

// Returns path to new Sass file for the following edition
// args:
//   edition: string -> name of the edition
async function createSassFile(edition) {
  const content =
    "$primary-color: " +
    JSON[edition]["palette-primary"] +
    ";\n" +
    "$secondary-color: " +
    JSON[edition]["palette-secondary"] +
    ";"
  const path = "assets/_" + edition + ".scss"

  fs.outputFile(path, content, function(err) {
    if (err) {
      return console.log(err)
    }
    console.log("Sass file for edition: " + edition + " was saved!")
  })

  return path
}

// Returns path to new Vue file
// args:
//   edition: string -> name of the edition
//   sassFilePath: string -> path to .scss file created for the edition
async function createVueFile(edition, sassFilePath) {
  path = "pages/" + edition + ".vue"

  await fs.copy("pages/_deploy_edition.vue", path)
  await fs.ensureFile(path)

  let html = await fs.readFile(path, "utf-8")
  html += STYLE_HEADER + ("@import '../" + sassFilePath + "';\n") + STYLE_ENDING

  await fs.outputFile(path, html)
  console.log("Vue file for edition: " + edition + " was saved!")

  return path
}

function afterDeploy() {
  for (edition in JSON) {
    fs.remove("assets/_" + edition + ".scss")
    fs.remove("pages/" + edition + ".vue")
  }
}

deploy()
